/*
 * Copyright (c) 2017 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

/dts-v1/;
#include <dt-bindings/input/input.h>
#include <dt-bindings/pwm/pwm.h>
#include <dt-bindings/display/media-bus-format.h>
#include "rk3288.dtsi"
#include "rk3288-android.dtsi"
//#include "lcd-rpdzkj-hdmi.dtsi"
//#include "lcd-rpdzkj-lvds-10_1_1024_600.dtsi"
//#include "lcd-rpdzkj_dual_lvds.dtsi"
#include "lcd-rpdzkj-mipi-10_1_1920_1200.dtsi"
//#include "lcd-rpdzkj-mipi-7_800_1280.dtsi"
//#include "lcd-rpdzkj-mipi-5_720_1280_old.dtsi"
//#include "lcd-rpdzkj-mipi-5_720_1280.dtsi"

//#include "lcd-edp.dtsi"

/ {
	model = "rp-rk3288";
	compatible = "rockchip,rp-rk3288", "rockchip,rk3288";
	memory {
		device_type = "memory";
		reg = <0x0 0x0 0x0 0x80000000>;
	};
	chosen {
		bootargs = "earlycon=uart8250,mmio32,0xff690000 swiotlb=1";
	};
	

	ext_gmac: external-gmac-clock {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <125000000>;
		clock-output-names = "ext_gmac";
	};

	vbat_wl: vcc_sys: vsys-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_sys";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
		regulator-boot-on;
	};

	vcc_sd: sdmmc-regulator {
		compatible = "regulator-fixed";
		gpio = <&gpio7 11 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default";
		pinctrl-0 = <&sdmmc_pwr>;
		regulator-name = "vcc_sd";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		startup-delay-us = <100000>;
		vin-supply = <&vcc_io>;
	};

	vcc_flash: flash-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_flash";
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&vcc_io>;
	};

	vcc_5v: usb-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_5v";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
		regulator-boot-on;
		vin-supply = <&vcc_sys>;
	};

	vcc_host_5v: usb-host-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcc_host_5v";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		regulator-always-on;
		vin-supply = <&vcc_5v>;
	};

	vcca_codec_V28: usb-host-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vcca_codec_V28";
		regulator-min-microvolt = <2800000>;
		regulator-max-microvolt = <2800000>;
		regulator-always-on;
		vin-supply = <&vcca_codec>;
	};


	/*
	 * A TT8142 creates both dovdd_1v8 and vcc28_dvp, controlled
	 * by the dvp_pwr pin.
	 */
//	vcc18_dvp: vcc28-dvp-regulator {
//		compatible = "regulator-fixed";
//		enable-active-high;
//		gpio = <&gpio0 17 GPIO_ACTIVE_HIGH>;
//		pinctrl-names = "default";
//		pinctrl-0 = <&dvp_pwr>;
//		regulator-name = "vcc18_dvp";
//		regulator-min-microvolt = <1800000>;
//		regulator-max-microvolt = <1800000>;
//		regulator-always-on;
//		vin-supply = <&vcc_sys>;
//	};

	wireless-bluetooth {
		compatible = "bluetooth-platdata";
//		clocks = <&rk808 1>;
//                clock-names = "ext_clock";
		uart_rts_gpios = <&gpio4 19 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default", "rts_gpio";
		pinctrl-0 = <&uart0_rts>;
		pinctrl-1 = <&uart0_gpios>;
		BT,reset_gpio    = <&gpio4 29 GPIO_ACTIVE_HIGH>;
		BT,wake_gpio     = <&gpio4 26 GPIO_ACTIVE_HIGH>;
		BT,wake_host_irq = <&gpio4 31 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};
	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
//		clocks = <&hym8563>;
//		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_enable_h>;

		/*
		 * On the module itself this is one of these (depending
		 * on the actual card populated):
		 * - SDIO_RESET_L_WL_REG_ON
		 * - PDN (power down when low)
		 */
		reset-gpios = <&gpio4 28 GPIO_ACTIVE_LOW>;
	};

	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		wifi_chip_type = "ap6335";
		sdio_vref = <1800>;
		WIFI,host_wake_irq = <&gpio4 30 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};
};

&emmc {
	bus-width = <8>;
	cap-mmc-highspeed;
	disable-wp;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&emmc_clk>, <&emmc_cmd>, <&emmc_pwr>, <&emmc_bus8>;
	vmmc-supply = <&vcc_io>;
	vqmmc-supply = <&vcc_flash>;
	status = "okay";
};

&gmac {
	assigned-clocks = <&cru SCLK_MAC>;
	assigned-clock-parents = <&ext_gmac>;
	clock_in_out = "input";
	pinctrl-names = "default";
	pinctrl-0 = <&rgmii_pins>, <&phy_rst>, <&phy_pmeb>, <&phy_int>;
	phy-supply = <&vcc_pmu>;
	phy-mode = "rgmii";
	snps,reset-active-low;
	snps,reset-delays-us = <0 10000 1000000>;
	snps,reset-gpio = <&gpio4 8 GPIO_ACTIVE_LOW>;
	tx_delay = <0x30>;
	rx_delay = <0x10>;
        //status = "disabled";
	status = "okay";
};

&gpu {
	mali-supply = <&vdd_gpu>;
	status = "okay";
};
/*
&hdmi {
	#address-cells = <1>;
	#size-cells = <0>;
	#sound-dai-cells = <0>;
	status = "okay";
};
*/
&hevc_service {
	status = "okay";
};

&i2c0 {
	clock-frequency = <400000>;
	status = "okay";

	vdd_cpu: syr827@40 {
		compatible = "silergy,syr827";
		fcs,suspend-voltage-selector = <1>;
		reg = <0x40>;
		regulator-name = "vdd_cpu";
		regulator-min-microvolt = <850000>;
		regulator-max-microvolt = <1350000>;
		regulator-always-on;
		regulator-boot-on;
		regulator-enable-ramp-delay = <300>;
		regulator-ramp-delay = <8000>;
		vin-supply = <&vcc_sys>;
		regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	vdd_gpu: syr828@41 {
		compatible = "silergy,syr828";
		fcs,suspend-voltage-selector = <1>;
		reg = <0x41>;
		regulator-name = "vdd_gpu";
		regulator-min-microvolt = <850000>;
		regulator-max-microvolt = <1350000>;
		regulator-always-on;
		regulator-ramp-delay = <6000>;
		vin-supply = <&vcc_sys>;
		regulator-state-mem {
			regulator-off-in-suspend;
		};
	};

	act8846: act8846@5a {
		compatible = "active-semi,act8846";
		reg = <0x5a>;
		status = "okay";
		pinctrl-names = "default";
		//pinctrl-0 = <&pmic_vsel>, <&pmic_sleep>, <&pwr_hold>;
		pinctrl-0 = <&pmic_vsel>, <&pwr_hold>;
		system-power-controller;

		vp1-supply = <&vcc_sys>;
		vp2-supply = <&vcc_sys>;
		vp3-supply = <&vcc_sys>;
		vp4-supply = <&vcc_sys>;
		inl1-supply = <&vcc_io>;
		inl2-supply = <&vcc_sys>;
		inl3-supply = <&vcc_20>;

		regulators {
			vcc_ddr: REG1 {
				regulator-name = "vcc_ddr";
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <1200000>;
				regulator-always-on;
			};

			vcc_io: REG2 {
				regulator-name = "vcc_io";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd_log: REG3 {
				regulator-name = "vdd_log";
				regulator-min-microvolt = <1100000>;
				regulator-max-microvolt = <1100000>;
				regulator-always-on;
			};

			vcc_20: REG4 {
				regulator-name = "vcc_20";
				regulator-min-microvolt = <2000000>;
				regulator-max-microvolt = <2000000>;
				regulator-always-on;
			};

			vccio_sd: REG5 {
				regulator-name = "vccio_sd";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd10_lcd: REG6 {
				regulator-name = "vdd10_lcd";
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-always-on;
			};

			vcca_codec: REG7 {
				regulator-name = "vcca_codec";
				//regulator-min-microvolt = <1800000>;
				//regulator-max-microvolt = <1800000>;
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
				//regulator-max-microvolt = <3300000>;
			};

			vcc_tp: REG8 {
				regulator-name = "vcc_tp";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vcc_pmu: REG9 {
				regulator-name = "vcc_pmu";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-always-on;
			};

			vdd_10: REG10 {
				regulator-name = "vdd_10";
				regulator-min-microvolt = <1000000>;
				regulator-max-microvolt = <1000000>;
				regulator-always-on;
			};

			vccio_wl: vcc_18: REG11 {
				regulator-name = "vcc_18";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};

			vcc18_lcd: REG12 {
				regulator-name = "vcc18_lcd";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};
		};
	};
};

&i2c3 {
	status = "okay";
};

&i2c4 {
	status = "okay";
};


&i2c5 {
        status = "disabled";
};

&io_domains {
	status = "okay";

	audio-supply = <&vcca_codec>;

	bb-supply = <&vcc_io>;
//	dvp-supply = <&vcc18_dvp>;
	flash0-supply = <&vcc_flash>;
	flash1-supply = <&vcc_io>;
	gpio30-supply = <&vcc_pmu>;
	gpio1830-supply = <&vcc_io>;
	lcdc-supply = <&vcc_io>;
	sdcard-supply = <&vccio_sd>;
	wifi-supply = <&vccio_wl>;
};

&rga {
	status = "okay";
};

&pinctrl {
	pcfg_output_high: pcfg-output-high {
		output-high;
	};

	pcfg_output_low: pcfg-output-low {
		output-low;
	};

	pcfg_pull_none_drv_8ma: pcfg-pull-none-drv-8ma {
		drive-strength = <8>;
	};

	pcfg_pull_up_drv_8ma: pcfg-pull-up-drv-8ma {
		bias-pull-up;
		drive-strength = <8>;
	};
	
	act8846 {
                pmic_vsel: pmic-vsel {
                        rockchip,pins = <7 10 RK_FUNC_GPIO &pcfg_output_low>;
                };
//		pmic_sleep: pmic-sleep {
//			rockchip,pins = <0 0 RK_FUNC_GPIO &pcfg_output_low>;
//		};
		pwr_hold: pwr-hold {
			rockchip,pins = <0 10 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	dvp {
		dvp_pwr: dvp-pwr {
			rockchip,pins = <0 11 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	gmac {
		phy_int: phy-int {
			rockchip,pins = <0 9 RK_FUNC_GPIO &pcfg_pull_up>;
		};

		phy_pmeb: phy-pmeb {
			rockchip,pins = <0 8 RK_FUNC_GPIO &pcfg_pull_up>;
		};

		phy_rst: phy-rst {
			rockchip,pins = <4 8 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins = <4 28 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	sdmmc {
		sdmmc_bus4: sdmmc-bus4 {
			rockchip,pins = <6 16 RK_FUNC_1 &pcfg_pull_up_drv_8ma>,
					<6 17 RK_FUNC_1 &pcfg_pull_up_drv_8ma>,
					<6 18 RK_FUNC_1 &pcfg_pull_up_drv_8ma>,
					<6 19 RK_FUNC_1 &pcfg_pull_up_drv_8ma>;
		};

		sdmmc_clk: sdmmc-clk {
			rockchip,pins = <6 20 RK_FUNC_1 &pcfg_pull_none_drv_8ma>;
		};

		sdmmc_cmd: sdmmc-cmd {
			rockchip,pins = <6 21 RK_FUNC_1 &pcfg_pull_up_drv_8ma>;
		};

		sdmmc_pwr: sdmmc-pwr {
			rockchip,pins = <7 11 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	usb_host {
		usbhub_rst: usbhub-rst {
			rockchip,pins = <5 13 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	wireless-bluetooth {
		uart0_gpios: uart0-gpios {
			rockchip,pins = <4 19 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};

&isp {
	status = "okay";
};

&isp_mmu {
	status = "okay";
};


&sdio0 {
	clock-frequency = <50000000>;
	clock-freq-min-max = <200000 50000000>;

	bus-width = <4>;
	cap-sd-highspeed;
	cap-sdio-irq;
	disable-wp;
	keep-power-in-suspend;
	mmc-pwrseq = <&sdio_pwrseq>;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdio0_bus4 &sdio0_cmd &sdio0_clk &sdio0_int>;
	sd-uhs-sdr104;
	supports-sdio;
	vmmc-supply = <&vbat_wl>;
	vqmmc-supply = <&vccio_wl>;
	status = "okay";
};

&sdmmc {
	bus-width = <4>;
	cap-mmc-highspeed;
	cap-sd-highspeed;
	card-detect-delay = <200>;
	disable-wp;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdmmc_clk>, <&sdmmc_cmd>, <&sdmmc_cd>, <&sdmmc_bus4>;
	supports-sd;
	vmmc-supply = <&vcc_sd>;
	vqmmc-supply = <&vccio_sd>;
	status = "okay";
};

&usbphy {
	status = "okay";
};

&usb_host1 {
	status = "okay";
};

&usb_host0_ehci {
	pinctrl-names = "default";
	pinctrl-0 = <&usbhub_rst>;
	status = "okay";
};

&vpu_service {
	status = "okay";
};
&rockchip_suspend {
        status = "okay";
        //status = "disabled";
        /*rockchip,pwm-regulator-config = <
                (0
                | PWM2_REGULATOR_EN
                )
        >;*/
        rockchip,power-ctrl =
                <&gpio0 0 GPIO_ACTIVE_LOW>;


};
